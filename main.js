'use strict';

var mandarinTimer;
var mandarinDelay = 2000;
var amount = 0;
var mandarin = document.getElementById('mandarin');
var buttonStart = document.getElementById('button');
// var level = document.getElementsByName('level');
var timeText = document.getElementById('timeText');
var resultText = document.getElementById('resultText');
var imagesArray = [
    'url("styles/images/owl.svg")',
    'url("styles/images/santa.svg")',
    'url("styles/images/gift.svg")',
    'url("styles/images/hat.svg")',
    'url("styles/images/xmas.svg")',
    'url("styles/images/gingerbread.svg")',
];

function startGame() {
    buttonStart.addEventListener('click', function () {
        buttonStart.innerText = 'Reload';
        buttonStart.addEventListener('click', function () {
            location.reload();
        });
        mandarinPosition();
        countGameDuration();
        mandarinJumping();
    });
}
startGame();

function mandarinPosition() {
    mandarin.style.display = 'block';
    mandarin.style.top = Math.round (Math.random() * 90) + '%';
    mandarin.style.left = Math.round (Math.random() * 90) + '%';
}

function mandarinJumping() {
    mandarinTimer = setInterval(function () {
        mandarinPosition();
    }, mandarinDelay);
}

mandarin.addEventListener('click', function () {
    clearInterval(mandarinTimer);
    changeImage();
    mandarinPosition();
    ++amount;
    resultText.innerText = 'Hey! You collected ' + amount + ' mandarins!';
});

function changeImage() {
    var imageNumber = Math.round(Math.random() * 6);
    mandarin.style.backgroundImage = imagesArray[imageNumber];
}

// function changeDifficultyLevel() {
//     level.addEventListener('checked', function () {
//
//     })
// }

function countGameDuration() {
    var gameTime = 10;
    var startTime = new Date().getTime();
    var durationTimer = setInterval(function () {
        var currentTime = new Date().getTime();
        var difference = currentTime - startTime;
        var seconds = Math.round(difference / 1000);
        if (seconds === gameTime){
            clearInterval(mandarinTimer);
            clearInterval(durationTimer);
            timeText.innerText = 'Time is over';
            mandarin.style.display = 'none';
        } else {
            timeText.innerText = gameTime - seconds;
        }
    }, mandarinDelay);
}